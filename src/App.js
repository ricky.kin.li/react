function MyButton() {
  return (
    <button>I'm a button</button>
  );
}

function AboutPage() {
  return (
    <>
    <h1>About Me</h1>
    <p>Hello there.<br />How are you?</p>
    </>
  )
}


export default function MyApp() {
  return (
    <div>
      <h1>Testing!</h1>
      <MyButton />
      <AboutPage />
    </div>
  );
}